Name: ca_FunnyCA
Version: 2.2
Release: 2%{?dist}
Source: ca_FunnyCA-%{version}.tgz
Prefix: /etc/grid-security/certificates
License: None
#BuildRequires: openssl
AutoReqProv: no
BuildRoot: %{_tmppath}/%{name}-root
BuildArch: noarch
URL: https://gitlab.cern.ch/ai-config-team/ca_FunnyCA

Summary: Trust anchors for starman only unacredited authority FunnyCA

%prep
%setup 

%description
This is the trust anchor for the FunnyCA.

You should install this package if you want to trust the identity 
assertions issued by the FunnyCA at CERN
Several instances of this package, corresponding to different CA's, can 
be installed simultaneously on the same system. 

%build


%install

mkdir -p $RPM_BUILD_ROOT/etc/grid-security/certificates

cp -pr *.0  *.signing_policy FunnyCA.crl_url  FunnyCA.pem  FunnyCA.signing_policy $RPM_BUILD_ROOT/etc/grid-security/certificates/

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
/etc/grid-security/certificates/

%changelog
* Mon Aug 07 2017 David Moreno Garcia <david.mogar@cern.ch> - 2.1-1
- Update symbolic links

* Mon Aug 07 2017 David Moreno Garcia <david.mogar@cern.ch> - 2.1-1
- Update ca certificate

* Mon Aug 07 2017 David Moreno Garcia <david.mogar@cern.ch> - 2.0-1
- Update certificates

* Wed Mar 30 2016 Steve Traylen <steve.traylen@cern.ch> - 1.0-1
- First version


